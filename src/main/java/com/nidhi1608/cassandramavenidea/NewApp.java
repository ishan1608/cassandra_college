package com.nidhi1608.cassandramavenidea;

import com.datastax.driver.core.*;

import java.util.Set;

/**
 * Created by ishan on 15/5/16.
 */
public class NewApp {
    public static void main(String[] args) {
        final Cluster.Builder clusterBuilder = Cluster.builder()
                .addContactPoints(
//                        "52.26.94.139" // AWS_VPC_US_WEST_2 (Amazon Web Services (VPC))
                        "52.36.119.153", "52.26.94.139", "52.26.136.104" // AWS_VPC_US_WEST_2 (Amazon Web Services (VPC))
                )
//                .withLoadBalancingPolicy(new DCAwareRoundRobinPolicy("AWS_VPC_US_WEST_2")) // your local data centre
                .withPort(9042)
                .withAuthProvider(new PlainTextAuthProvider("iccassandra", "f70cb6d4e05157627925d5599e4b7be9"));

        final Cluster cluster = clusterBuilder.build();
        final Metadata metadata = cluster.getMetadata();
        System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());

        for (final Host host: metadata.getAllHosts()) {
            System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
        }
//        No need for extracting this host
//        Set<Host> hosts = metadata.getAllHosts();
//        Host host = hosts.iterator().next();

        String keyspace = "health";
        Session session = cluster.connect(keyspace);
// Create a table 'users'
        session.execute("CREATE TABLE IF NOT EXISTS demo (firstname text, lastname text, age int, email text, city text, PRIMARY KEY (lastname))");

        // Insert one record into the users table
        session.execute("INSERT INTO demo (lastname, age, city, email, firstname) VALUES ('Prasad', 35, 'Patna', 'pari@example.com', 'Pari')");

        // Use select to get the user we just entered
        ResultSet results = session.execute("SELECT * FROM demo WHERE lastname='Jones'");
        for (Row row : results) {
            System.out.format("%s %d\n", row.getString("firstname"), row.getInt("age"));
        }

        // Update the same user with a new age
        session.execute("update demo set age = 36 where lastname = 'Jones'");
        // Select and show the change
        results = session.execute("select * from demo where lastname='Jones'");
        for (Row row : results) {
            System.out.format("%s %d\n", row.getString("firstname"), row.getInt("age"));
        }

        // Delete the user from the users table
//        session.execute("DELETE FROM users WHERE lastname = 'Jones'");
        // Show that the user is gone
        results = session.execute("SELECT * FROM users");
        for (Row row : results) {
            System.out.format("%s %d %s %s %s\n", row.getString("lastname"), row.getInt("age"),  row.getString("city"), row.getString("email"), row.getString("firstname"));
        }

        // Clean up the connection by closing it
        cluster.close();
    }
}