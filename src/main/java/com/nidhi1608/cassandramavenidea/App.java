package com.nidhi1608.cassandramavenidea;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * Accessing cassandra with JAVA
 */
public class App
{
    public static void main( String[] args )
    {
        Cluster cluster;
        Session session;

        // Connect to the cluster and keyspace "demo"
        cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        session = cluster.connect("health");

        // Create a table 'users'
        session.execute("CREATE TABLE IF NOT EXISTS abc (firstname text, lastname text, age int, email text, city text, PRIMARY KEY (lastname))");

        // Insert one record into the users table
        session.execute("INSERT INTO abc (lastname, age, city, email, firstname) VALUES ('Kumar', 25, 'Austin', 'anshu@example.com', 'Anshu')");

        // Use select to get the user we just entered
        ResultSet results = session.execute("SELECT * FROM abc WHERE lastname='kumar'");
        for (Row row : results) {
            System.out.format("%s %d\n", row.getString("firstname"), row.getInt("age"));
        }

        // Update the same user with a new age
//        session.execute("update users set age = 36 where lastname = 'Jones'");
//        // Select and show the change
//        results = session.execute("select * from users where lastname='Jones'");
//        for (Row row : results) {
//            System.out.format("%s %d\n", row.getString("firstname"), row.getInt("age"));
//        }
//
//        // Delete the user from the users table
//        session.execute("DELETE FROM users WHERE lastname = 'Jones'");
//        // Show that the user is gone
//        results = session.execute("SELECT * FROM users");
//        for (Row row : results) {
//            System.out.format("%s %d %s %s %s\n", row.getString("lastname"), row.getInt("age"),  row.getString("city"), row.getString("email"), row.getString("firstname"));
//        }

        // Clean up the connection by closing it
        cluster.close();

        
    }
}