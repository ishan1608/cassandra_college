package com.nidhi1608.cassandramavenidea.options;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by nidhi on 17/5/16.
 */
public class PatientProvider {
    private ArrayList<Cell> maleNameList;
    private ArrayList<Cell> femaleNameList;
    private ArrayList<Cell> lastNameList;
    private ArrayList<Cell> occupationList;
    private ArrayList<Cell> cityList;
    private ArrayList<Cell> stateList;
    private ArrayList<Cell> countryList;
    private ArrayList<Cell> bloodGroupList;
    private ArrayList<Cell> relationList;

    private void initialize() {
        try {
            // Getting options.xlsx file from resources as InputStream
            InputStream namesFileInputStream = PatientProvider.class.getClassLoader().getResourceAsStream("options.xlsx");
            // Finds the workbook instance for XLSX file
            XSSFWorkbook optionsWorkbook = new XSSFWorkbook(namesFileInputStream);
            // Return male_name from the XLSX workbook
            XSSFSheet maleNameSheet = optionsWorkbook.getSheet("male_name");
            // Caching the data in a List
            maleNameList = new ArrayList<Cell>();
            for (Row row : maleNameSheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                maleNameList.add(cell);
            }

            // Repeating the same for female names
            XSSFSheet femaleNameSheet = optionsWorkbook.getSheet("female_name");
            // Caching the data in a List
            femaleNameList = new ArrayList<Cell>();
            for (Row row : femaleNameSheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                femaleNameList.add(cell);
            }

            // Repeating the same for last names
            XSSFSheet lastNameSheet = optionsWorkbook.getSheet("last_name");
            // Caching the data in a list
            lastNameList = new ArrayList<Cell>();
            for (Row row : lastNameSheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                lastNameList.add(cell);
            }
            // Repeating the same for occupations
            XSSFSheet occupationSheet = optionsWorkbook.getSheet("occupations");
            // Caching the data in a list
            occupationList = new ArrayList<Cell>();
            for (Row row : occupationSheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                occupationList.add(cell);
            }

            // Repeating the same for cities
            XSSFSheet citySheet = optionsWorkbook.getSheet("cities");
            // Caching the data in a list
            cityList = new ArrayList<Cell>();
            for (Row row : citySheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                cityList.add(cell);
            }

            // Repeating the same for states
            XSSFSheet stateSheet = optionsWorkbook.getSheet("states");
            // Caching the data in a list
            stateList = new ArrayList<Cell>();
            for (Row row : stateSheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                stateList.add(cell);
            }

            // Repeating the same for countries
            XSSFSheet countrySheet = optionsWorkbook.getSheet("countries");
            // Caching the data in a list
            countryList = new ArrayList<Cell>();
            for (Row row : countrySheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                countryList.add(cell);
            }

            // Repeating the same for bloodGroup
            XSSFSheet bloodGroupSheet = optionsWorkbook.getSheet("blood_groups");
            // Caching the data in a list
            bloodGroupList = new ArrayList<Cell>();
            for (Row row : bloodGroupSheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                bloodGroupList.add(cell);
            }

            // Repeating the same for relation
            XSSFSheet relationSheet = optionsWorkbook.getSheet("relations");
            // Caching the data in a list
            relationList = new ArrayList<Cell>();
            for (Row row : relationSheet) {
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                relationList.add(cell);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public class Patient {

        String firstName = null;
        String lastName = null;
        int age = -1;
        String gender = null;
        String occupation = null;
        String district = null;
        String city = null;
        String state = null;
        String pin = null;
        String country = null;
        String address = null;
        String email = null;
        String phone = null;
        String phone2 = null;
        String bloodGroup = null;
        String relation = null;

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getName() {
            return String.format("%s %s", firstName, lastName);
        }

        public int getAge() {
            return age;
        }

        public String getGender() {
            return gender;
        }

        public String getOccupation() {
            return occupation;
        }
        public String getDistrict() {
            return district;
        }

        public String getCity() {
            return city;
        }

        public String getState() {
            return state;
        }

        public String getPin() {
            return pin;
        }

        public String getCountry() {
            return country;
        }

        public String getAddress() {
            return address;
        }

        public String getEmail() {
            return email.toLowerCase();
        }

        public String getPhone() {
            return phone;
        }

        public String getPhone2() {
            return phone2;
        }

        public String getBloodGroup() {
            return bloodGroup;
        }

        public String getRelation() {
            return relation;
        }

        public String getDescription() {
            return String.format("%s, %d is a %s %s and can be contacted at %s, %s, %s, %s. Can have a relation as %s." +
                    "Emergency will require %s",
                    getName(), getAge(), getGender(), getOccupation(), getEmail(), getPhone(), getPhone2(),
                    getAddress(), getRelation(), getBloodGroup());
        }
    }

    public Patient getPatient() {
        if (maleNameList == null) {
            initialize();
        }
        Random random = new Random();
        Patient patient = new Patient();
        if (random.nextInt(2) == 1) {
            patient.gender = "Male";
            int pos = random.nextInt(maleNameList.size());
            patient.firstName = maleNameList.get(pos).getStringCellValue();
        } else {
            patient.gender = "Female";
            int pos = random.nextInt(femaleNameList.size());
            patient.firstName = femaleNameList.get(pos).getStringCellValue();
        }
        int pos = random.nextInt(lastNameList.size());
        patient.lastName = lastNameList.get(pos).getStringCellValue();
        // Setting random mage with max 100
        patient.age = random.nextInt(101);
        // Setting occupation
        pos = random.nextInt(occupationList.size());
        patient.occupation = occupationList.get(pos).getStringCellValue();
        // Setting district
        pos = random.nextInt(cityList.size());
        patient.district = cityList.get(pos).getStringCellValue();
        // Setting city
        pos = random.nextInt(cityList.size());
        patient.city = cityList.get(pos).getStringCellValue();
        // Setting state
        pos = random.nextInt(stateList.size());
        patient.state = stateList.get(pos).getStringCellValue();
        // Setting pin
        int sdc = 10;
        patient.pin = String.format("%d%d%d%d%d%d", random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc),
                random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc));
        // Setting country
        pos = random.nextInt(countryList.size());
        patient.country = countryList.get(pos).getStringCellValue();
        // Setting address
        patient.address = String.format("%s %s %s %s %s", patient.getDistrict(), patient.getCity(), patient.getState(), patient.getCountry(),
                patient.getPin());
        // Setting email
        patient.email = String.format("%s_%s@gmail.com", patient.getFirstName(), patient.getLastName());
        // Setting phone
        patient.phone = String.format("%d%d%d%d%d%d%d%d%d%d", random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc),
                random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc),
                random.nextInt(sdc), random.nextInt(sdc));
        // Setting phone2
        patient.phone2 = String.format("%d%d%d%d%d%d%d%d%d%d", random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc),
                random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc), random.nextInt(sdc),
                random.nextInt(sdc), random.nextInt(sdc));
        // Setting Blood Group
        pos = random.nextInt(bloodGroupList.size());
        patient.bloodGroup = bloodGroupList.get(pos).getStringCellValue();
        // Setting Relation
        pos = random.nextInt(relationList.size());
        patient.relation = relationList.get(pos).getStringCellValue();
        return patient;
    }
}

