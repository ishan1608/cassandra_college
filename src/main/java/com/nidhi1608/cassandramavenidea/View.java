package com.nidhi1608.cassandramavenidea;

import com.datastax.driver.core.*;

/**
 * Created by nidhi on 17/5/16.
 */
public class View {
    public static void main(String[] args) {
        final Cluster.Builder clusterBuilder = Cluster.builder()
                .addContactPoint(
//                        "52.26.94.139" // AWS_VPC_US_WEST_2 (Amazon Web Services (VPC))
                        "52.26.94.139" // AWS_VPC_US_WEST_2 (Amazon Web Services (VPC))
                )
//                .withLoadBalancingPolicy(new DCAwareRoundRobinPolicy("AWS_VPC_US_WEST_2")) // your local data centre
                .withPort(9042)
                .withAuthProvider(new PlainTextAuthProvider("iccassandra", "f70cb6d4e05157627925d5599e4b7be9"));

        final Cluster cluster = clusterBuilder.build();
        final Metadata metadata = cluster.getMetadata();
        System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());

        for (final Host host : metadata.getAllHosts()) {
            System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
        }
//        No need for extracting this host
//        Set<Host> hosts = metadata.getAllHosts();
//        Host host = hosts.iterator().next();

        String keyspace = "health_record";
        Session session = cluster.connect(keyspace);

        ResultSet results = session.execute("SELECT * FROM patient");
        for (Row row : results) {
            System.out.format("%s %s %s %s\n", row.getString("pt_name"), row.getString("pt_gender"), row.getString("pt_occupation"), row.getString("pt_address"));
        }

        cluster.close();
    }
}
