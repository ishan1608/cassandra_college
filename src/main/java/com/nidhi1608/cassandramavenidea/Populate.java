package com.nidhi1608.cassandramavenidea;

import com.datastax.driver.core.*;
import com.datastax.driver.core.utils.UUIDs;
import com.nidhi1608.cassandramavenidea.options.PatientProvider;

import java.util.Calendar;

/**
 * Created by nidhi on 17/5/16.
 */
public class Populate {
    static int numberOfRecords = 10;

    public static void main(String[] args) {
        final Cluster.Builder clusterBuilder = Cluster.builder()
                .addContactPoint(
//                        "52.26.94.139" // AWS_VPC_US_WEST_2 (Amazon Web Services (VPC))
                        "52.26.94.139" // AWS_VPC_US_WEST_2 (Amazon Web Services (VPC))
                )
//                .withLoadBalancingPolicy(new DCAwareRoundRobinPolicy("AWS_VPC_US_WEST_2")) // your local data centre
                .withPort(9042)
                .withAuthProvider(new PlainTextAuthProvider("iccassandra", "f70cb6d4e05157627925d5599e4b7be9"));

        final Cluster cluster = clusterBuilder.build();
        final Metadata metadata = cluster.getMetadata();
        System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());

        for (final Host host : metadata.getAllHosts()) {
            System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
        }
//        No need for extracting this host
//        Set<Host> hosts = metadata.getAllHosts();
//        Host host = hosts.iterator().next();

        String keyspace = "health_record";
        Session session = cluster.connect(keyspace);

//        session.execute("INSERT INTO patient (patient_id, pt_name, pt_gender, pt_occupation, pt_address, pt_city_town_vill, pt_district, pt_state, pt_pin, pt_country, pt_email_id, pt_phone, pt_phone2, ecp_name, ecp_relation, ecp_address, ecp_city_town_vill, ecp_district, ecp_state, ecp_pin, ecp_country, ecp_email_id, ecp_phone, blood_grp) VALUES (uuid(), 'nidhi','female', 'student', 'saltlake', 'kolkata', 'northpragana', 'west bengal', '700064', 'india', 'nid@gmail.com', '8443767992', '7696470467', 'Anshu', 'brother', 'nanda', 'delhi', 'new delhi', 'old delhi', '700094', 'india', 'anshu@gmail.com', '9988776655', 'O positive')");
//
//        session.execute("INSERT INTO doctor (dr_specialization, dr_id, doctor_name, dr_organization, dr_address, dr_city_town_vill, dr_district, dr_state, dr_pin, dr_country, dr_email_id, dr_phone, dr_phone2) VALUES ('Gynaecologist', uuid(), 'Reeta Dhingra', ['Apolo','Nanda'], 'Dwarka', 'Delhi', 'New Delhi', 'New Delhi', '700063', 'india', 'reeta@gmail.com', '7766554433', '2233445566')");
//
//        session.execute("INSERT INTO treatments (trt_id, pt_id, pt_name, pt_addr, pt_gender, pt_fd_hbt, pt_allergy, blood_grp, dr_id, dr_name, dr_specialization, is_active, complaint, diagnosis, clinical_status) VALUES(uuid(), uuid(), 'nidhi', 'saltlake', 'female', 'sweets', {'sour': 'nuts'}, 'O positive', uuid(), 'Reeta Dhingra', 'Gynaecologist', TRUE, 'Pcod', 'pcod', 'First Level')");
//
//        session.execute("INSERT INTO visits (visit_id, pt_id, pt_name, pt_add, pt_food_habit, allergy, blood_grp, trt_id, initial_complaint, reason, systolic_bp, diastolic_bp, pulse_rate, temp, respiration_rate, height_cms, weight_kgs, investigations, investigation_reports, medication, diagnosis, current_status) VALUES (uuid(), uuid(), 'nidhi', 'saltlake', 'sweets', {'brinjal': 'peanuts'}, 'O positive', uuid(), 'pcod', 'period', 120, 80, 72, 98.6, 44, 153, 50, ['ovary', 'uterus'], {'abhd': 'nbhubu'}, {'lkjh': 'ghjkl'}, 'medicines', 'good')");
        PatientProvider patientProvider = new PatientProvider();

        PreparedStatement statement = session.prepare("INSERT INTO patient" + "(patient_id, pt_name, pt_gender, " +
                "pt_occupation, pt_address, pt_city_town_vill, pt_district, pt_state, pt_pin, pt_country, " +
                "pt_email_id, pt_phone, pt_phone2, ecp_name, ecp_relation, ecp_address, ecp_city_town_vill, " +
                "ecp_district, ecp_state, ecp_pin, ecp_country, ecp_email_id, ecp_phone, blood_grp)"
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

        BoundStatement boundStatement = new BoundStatement(statement);

        System.out.println(String.format("Adding %d record(s).", numberOfRecords));
        // Getting start time
        Calendar calendar = Calendar.getInstance();
        long startTime = calendar.getTimeInMillis();
//        System.out.println(startTime);
        // Actually executing query
        for (int i = 0; i < numberOfRecords; i++) {
            PatientProvider.Patient patient = patientProvider.getPatient();
            PatientProvider.Patient ecpatient = patientProvider.getPatient();
            session.execute(boundStatement.bind(UUIDs.random(), patient.getName(), patient.getGender(),
                    patient.getOccupation(), patient.getAddress(), patient.getCity(), patient.getDistrict(),
                    patient.getState(), patient.getPin(), patient.getCountry(), patient.getEmail(), patient.getPhone(),
                    patient.getPhone2(), ecpatient.getName(), patient.getRelation(), ecpatient.getAddress(),
                    ecpatient.getCity(), ecpatient.getDistrict(), ecpatient.getState(), ecpatient.getPin(),
                    ecpatient.getCountry(), ecpatient.getEmail(), ecpatient.getPhone(), patient.getBloodGroup()));
        }
        // Getting end time
        calendar = Calendar.getInstance();
        long endTime = calendar.getTimeInMillis();
//        System.out.println(endTime);
        System.out.println(String.format("Time to execute: %d milliseconds", endTime - startTime));

        cluster.close();
    }
}
