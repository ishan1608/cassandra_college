package com.nidhi1608.cassandramavenidea;

import com.datastax.driver.core.*;

import java.util.List;
import java.util.Map;

/**
 * Created by nidhi on 17/5/16.
 */
public class Provision {
    public static void main(String[] args) {
        final Cluster.Builder clusterBuilder = Cluster.builder()
                .addContactPoint(
//                        "52.26.94.139" // AWS_VPC_US_WEST_2 (Amazon Web Services (VPC))
                        "52.26.94.139" // AWS_VPC_US_WEST_2 (Amazon Web Services (VPC))
                )
//                .withLoadBalancingPolicy(new DCAwareRoundRobinPolicy("AWS_VPC_US_WEST_2")) // your local data centre
                .withPort(9042)
                .withAuthProvider(new PlainTextAuthProvider("iccassandra", "f70cb6d4e05157627925d5599e4b7be9"));

        final Cluster cluster = clusterBuilder.build();
        final Metadata metadata = cluster.getMetadata();
        System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());

        for (final Host host : metadata.getAllHosts()) {
            System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(), host.getRack());
        }
//        No need for extracting this host
//        Set<Host> hosts = metadata.getAllHosts();
//        Host host = hosts.iterator().next();

        String keyspace = "health_record";
        Session session = cluster.connect(keyspace);
// Create a table 'users'
        session.execute("CREATE TABLE IF NOT EXISTS patient (patient_id uuid, pt_name text, pt_gender text, pt_occupation text, pt_address text, pt_city_town_vill text, pt_district text, pt_state text, pt_pin text, pt_country text, pt_email_id text, pt_phone text, pt_phone2 text, ecp_name text, ecp_relation text, ecp_address text, ecp_city_town_vill text, ecp_district text, ecp_state text, ecp_pin text, ecp_country text, ecp_email_id text, ecp_phone text, blood_grp text, PRIMARY KEY (patient_id))");

        session.execute("CREATE TABLE IF NOT EXISTS doctor (dr_specialization text, dr_id uuid, doctor_name text, dr_organization list<text>, dr_address text, dr_city_town_vill text, dr_district text, dr_state text, dr_pin text, dr_country text, dr_email_id text, dr_phone text, dr_phone2 text, PRIMARY KEY (dr_specialization, dr_id))");

        session.execute("CREATE TABLE IF NOT EXISTS treatments (trt_id uuid, pt_id uuid, pt_name text, pt_addr text, pt_gender text, pt_fd_hbt text, pt_allergy map<text, text>, blood_grp text, dr_id uuid, dr_name text, dr_specialization text, is_active boolean, complaint text, diagnosis text, clinical_status text, PRIMARY KEY((pt_id), dr_id, trt_id))");
//
        session.execute("CREATE TABLE IF NOT EXISTS visits (visit_id uuid, pt_id uuid, pt_name text, pt_add text, pt_food_habit text, allergy map<text, text>, blood_grp text, trt_id uuid, initial_complaint text, reason text, systolic_bp int, diastolic_bp int, pulse_rate int, temp float, respiration_rate int, height_cms int, weight_kgs float, investigations list<text>, investigation_reports map<text, text>, medication map<text, text>, diagnosis text, current_status text, PRIMARY KEY ((pt_id), trt_id, visit_id))");

//        session.execute("USE health_record");
//        session.execute("DESC TABLE patient");
        cluster.close();
    }
}
