#Getting Started

Get yourself acquainted with Developer walkthrough of [Try Cassandra](http://www.planetcassandra.org/try-cassandra/)

##Installing Cassandra
Go to [DATASTAX Enterprise](https://academy.datastax.com/downloads/welcome) and download the **Package** (not ~~Sandbox~~) for Linux.

Once downloaded make the file executable ```chmod +x DataStaxEnterprise-4.8.6.2016033119-linux-x64-installer.run```

Start the installer with **admin previliages** ```sudo ./DataStaxEnterprise-4.8.6.2016033119-linux-x64-installer.run```

Select Advance Installation, follow the Wizard to complete installation. See [install config](install_config/datastax_enterprise_installation_config) for the configuration options that I chose.

Run ```nodetool status``` to see if cassandra has been installed successfully. The output should be similar to this

    Datacenter: Cassandra
    =====================
    Status=Up/Down
    |/ State=Normal/Leaving/Joining/Moving
    --  Address    Load       Tokens  Owns    Host ID                               Rack
    UN  127.0.0.1  723.31 KB  256     ?       75a8a17b-2e5d-4890-b8d5-abfad4178ec3  rack1
    Note: Non-system keyspaces don't have the same replication settings, effective ownership information is meaningless

##Installing OpsCenter
Install OpsCenter the same way as cassandra from [DATASTAX OpsCenter](https://academy.datastax.com/downloads/ops-center).

Configuration options that I chose can be found [here](install_config/datastax_opscenter_installation_config)

To check the installation open [http://127.0.0.1:8888/opscenter/index.html](http://127.0.0.1:8888/opscenter/index.html), if it shows OpsCenter then it is successfuly installed.

##Installing DevCenter
Go to [DevCenter](https://academy.datastax.com/downloads/ops-center#devCenter) and select 64-bit Linux to download.
Installing DevCenter is as simple as extracting the tarball to your favourite location.

To extract use ```tar xvzf DevCenter-1.5.0-linux-gtk-x86_64.tar.gz```

To run DevCenter go to the directory you extracted and run the file **DevCenter** from GUI or via terminal like this ```./DevCenter```

###Using DevCenter
####Adding connection
**File > New Connection** or **Shift + Ctrl + Alt + N**

Enter a Connection Name. ```127.0.0.1``` in Connection hosts.

Leave Native Protocol port as it is (i.e. 9042).

Chose any compression type (I prefer LZ4 as it provides better compression ratio).

Click **Add** > _Try to establish a connection (optional)_ > **Finish**.

####Running scripts
DevCenter doesn't supports all cqlsh statements and nor does the Java Driver(Installation is further below in this document).

Some scripts that can run on DevCenter can be found in [cql_scripts](cql_scripts) folder.
Open the *.cql files using any text editor.
 
##Working with JAVA
**UPDATE This method requires running terminal commands. Autocomplete support for external dependencies in 
Sublime or Atom aren't as good as I would like them to be.

** To solve these problems I have added the steps to use [IntelliJ Idea](https://www.jetbrains.com/idea/) with cassandra
####Installing maven
To install maven ```sudo apt-get install maven```

To verify installation run ```mvn --version```

**Create and move to a folder where you want to keep all your source code.

####Creating a project
Run the command below to create your project replacing ```groudpId``` and ```artifactId``` appropriately.

```mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false``` 

Ex:

```mvn archetype:generate -DgroupId=com.ishan1608.cassandramaven -DartifactId=cassandra-maven -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false```

**Move to the newly created directory ```cd cassandra-maven```

####Adding drivers
Edit your ```pom.xml``` file and add the following lines in dependencies array to add drivers as dependency.

==================================================================

    <dependency>
      <groupId>com.datastax.cassandra</groupId>
      <artifactId>cassandra-driver-core</artifactId>
      <version>3.0.0</version>
    </dependency>
    <dependency>
      <groupId>com.datastax.cassandra</groupId>
      <artifactId>cassandra-driver-mapping</artifactId>
      <version>3.0.0</version>
    </dependency>
    <dependency>
      <groupId>com.datastax.cassandra</groupId>
      <artifactId>cassandra-driver-extras</artifactId>
      <version>3.0.0</version>
    </dependency>
    
==================================================================

####Packaging dependencies with Jar
Edit your ```pom.xml``` and add build configuration by adding these lines under ```project``` 

==================================================================

    <build>
        <plugins>
          <!-- maven assenmpy plugin to include dependencies -->
          <plugin>
            <artifactId>maven-assembly-plugin</artifactId>
            <executions>
              <execution>
                <phase>package</phase>
                <goals>
                  <goal>single</goal>
                </goals>
              </execution>
            </executions>
            <configuration>
              <descriptorRefs>
                <descriptorRef>jar-with-dependencies</descriptorRef>
              </descriptorRefs>
            </configuration>
          </plugin>
        </plugins>
      </build>
==================================================================

####Adding slf4j (Optional)
Edit your ```pom.xml``` file and add the following lines in dependencies array to add it as dependency.

==================================================================

    <!-- Optional -->
    <dependency>
       <groupId>org.slf4j</groupId>
       <artifactId>slf4j-api</artifactId>
       <version>1.7.5</version>
    </dependency>
    <dependency>
       <groupId>org.slf4j</groupId>
       <artifactId>slf4j-simple</artifactId>
       <version>1.6.4</version>
    </dependency>

==================================================================

####Writing JAVA code finally :)
Edit the file ```com.ishan1608.cassandramaven.App.java``` and copy the code from [App.java](cassandra-maven/src/main/java/com/ishan1608/cassandramaven/App.java)

####Packaging and running the code
run ```mvn clean package```

If there are no errors run ```java -cp target/cassandra-maven-1.0-SNAPSHOT-jar-with-dependencies.jar com.ishan1608.cassandramaven.App```

Hopefully everything ran correctly by now and you will see the output as

    Bob 35
    Bob 36
    
If you see the above output congratulations!, you succcessfully ran your first program using cassandra as database, JAVA as the implementaion language and Maven for dependency management.

##Working with IntelliJ Idea
####Install IntelliJ Idea Community Edition
Go to [Choose your edition](https://www.jetbrains.com/idea/#chooseYourEdition) and click on Download ensuring that your choice of platform (Linux) is selected.
When downloaded extract the files to a location where you like to keep your programs. Remember these files are required to run the IntelliJ Idea (editor) itself and not your programs, so keep it in a place it won't interfere with your daily work.

I like to place it in ```/opt``` Here is what I do:

    cd /opt
    sudo mkdir intellijIdea
    sudo chown {username} intellijIdea
    --copy and extract the tarball to this folder
    --move to bin folder
    --open idea.sh file

To open later you have to open the same **idea.sh** file. To avoid this, you can go to **Tools > Create Desktop Entry** inside the Editor. This will allow you to open IntelliJ Idea from the Applications Menu or by searching for it.

####Creating Project
Close any opened projects in the editor. Follow these steps:

***Create New Project***

**Maven (*Left menu*)**

*Select or browse for __Project SDK__*

*Check __Create from Archetype__ and select __org.apache.maven.archetypes:maven-archetype-quickstart__ click __Next__*

*__GroudId__ is your package(com.mycompany.app) Ex: com.ishan1608.cassandramavenidea*

*__Artifact Id__ is the same as above(my-app) Ex: cassandra-maven-idea*

*You can leave the version number as it is but I prefer [Semantic Versioning](http://semver.io/) Ex: 0.0.1 and click __Next__*

Leave these settings as it is, they are basically asking you which version of maven to use. The default should be *Bundled (Maven 3)*

Select a Project Name, a location and click on __Finish__

 If this is your first time, it may take some time to download. When finished you will be prompted ___Maven projects need to be imported___, click on **Enable Auto-Import**.
 
 In the left hand pane there is a **Project** panel, choose **Project Files** from the top drop-down. Edit _pom.xml_ and _App.java_ like I have mentioned above.
 
 #####Benefits of IntelliJ Idea
 Remove the import line from _App.java_ and click somewhere near Class names, which should have been imported. Editor will show you a small hint to press __Alt + Enter__. Pressing the same will add the import statement to the top of your file automatically.
 
 **Import Row class will provide you with multiple options(this is normal for classes with multiple declarations), choose the _com.datastax.driver.core_ one
 
 ####Configuring Run
 Go to **Run > Edit Configurations**
 
 *Click on the __+__ button on the left and choose __Application__*
 
 Here we can add the configurations to run our program. Provide a name for the configuration Ex: __App__. Browse for the _Main Class_ and choose your __App.java__ class. __Apply > OK__
 
 ####Running the program
 A new green colored _Run_ button will appear now on the right side of the toolbar. Clicking on it will run the program and an output will appear in the bottom pane of the editor.
 
 ####Configuring .gitignore
 Since we are using IntelliJ Idea it adds some files and folders of its own. Configuring .gitignore is important.
 
 Add these lines to your __.gitignore__ file in the root of your project:
 
     target
    .idea/workspace.xml
    .idea/tasks.xml

A repo showing all of this has been added [here](https://bitbucket.org/ishanatmuz/learning-cassandra)

**You are all set now with one click run of your program, best in industry auto-complete in your editor, with Git as your VCS.

Happy coding :)
